import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Node {
    private String data;
    private Node parent;
    private final Set<Node> children = new HashSet<>();

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setParent(Node parent) {
        if (this.parent != null && !this.parent.equals(parent)) {
            this.parent.getChildren().remove(this);
        }
        this.parent = parent;
        if (this.parent != null) {
            this.parent.getChildren().add(this);
        }
    }

    public Node getParent() {
        return parent;
    }

    public Set<Node> getChildren() {
        return children;
    }
}

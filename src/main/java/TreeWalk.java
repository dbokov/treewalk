import java.util.*;

public class TreeWalk {

    public static void main(String[] args) {
        Node rootNode = generateTree();
        TreeMap<Integer, Set<String>> nodesByLevels = new TreeMap<>();
        getLevels(rootNode, 0, nodesByLevels);
        for (Map.Entry<Integer, Set<String>> entry: nodesByLevels.descendingMap().entrySet()) {
            System.out.println(entry.getValue());
        }
    }

    // заполняем мэп значений по уровням
    // computeIsAbsent проверяет создан ли сет значений уровней, если нет - создает, затем добавляет текущее значение

    public static void getLevels(Node currentNode, int currentLevel, Map<Integer, Set<String>> nodesByLevels) {
        nodesByLevels.computeIfAbsent(currentLevel, integer -> new HashSet<>()).add(currentNode.getData());
        currentNode.getChildren().forEach(node -> getLevels(node, currentLevel + 1, nodesByLevels));
    }

    // вернем верхушку дерева
    // см. derevo.jpg
    public static Node generateTree() {
        ArrayList<Node> nodes = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            Node n = new Node();
            n.setData(String.valueOf(i + 1));
            nodes.add(n);
        }
        nodes.get(6).setParent(nodes.get(2));
        nodes.get(5).setParent(nodes.get(3));
        nodes.get(4).setParent(nodes.get(1));
        nodes.get(3).setParent(nodes.get(1));
        nodes.get(2).setParent(nodes.get(0));
        nodes.get(1).setParent(nodes.get(0));
        nodes.get(0).setParent(null);

        return nodes.get(0);
    }

}
